# Tool Attributions

The following is a list of the tools I used in the creation and modification of my program, along with each tool's license type and GitHub link:

Vermin: <https://github.com/netromdk/vermin>
License: MIT License

Ruff: <https://github.com/charliermarsh/ruff-vscode>
License: MIT License

pipdeptree: <https://github.com/tox-dev/pipdeptree>
License: MIT License

Black: <https://github.com/psf/black>
License: MIT License

## VSCode Extensions

autoDocstring: <https://github.com/NilsJPWerner/autoDocstring>
License: MIT License

Even Better TOML: <https://github.com/tamasfe/taplo>
License: MIT License

markdownlint: <https://github.com/DavidAnson/vscode-markdownlint>
License: MIT License
